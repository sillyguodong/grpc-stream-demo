package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	streamv1 "gitea.com/sillyguodong/grpc-stream-demo/gen/stream/v1"
	"gitea.com/sillyguodong/grpc-stream-demo/gen/stream/v1/streamv1connect"
	"github.com/bufbuild/connect-go"
)

func main() {
	mux := http.NewServeMux()
	mux.Handle(streamv1connect.NewStreamTestServiceHandler(&StreamTestService{}))
	mux.HandleFunc("/hello", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("hi\n"))
	})

	srv := &http.Server{
		Handler: mux,
		Addr:    "localhost:8080",
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, syscall.SIGTERM)
	go func() {
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("HTTP listen and serve: %v", err)
		}
	}()

	<-signals
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatalf("HTTP shutdown: %v", err)
	}
}

type StreamTestService struct {
	streamv1connect.UnimplementedStreamTestServiceHandler
}

func (svc *StreamTestService) StreamTest(ctx context.Context, req *connect.Request[streamv1.StreamTestRequest], stream *connect.ServerStream[streamv1.StreamTestResponse]) error {
	fmt.Printf("[server] start handle request, content: %s\n", req.Msg.Content)

	ticker := time.NewTicker(1 * time.Second)
	defer ticker.Stop()

	done := make(chan struct{})
	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				stream.Send(&streamv1.StreamTestResponse{Content: "grpc stream test resp"})
			}
		}
	}()

	time.Sleep(5 * time.Minute)
	done <- struct{}{}
	fmt.Println("[server] done")
	return nil
}
