module gitea.com/sillyguodong/grpc-stream-demo

go 1.20

require (
	github.com/bufbuild/connect-go v1.3.1
	google.golang.org/protobuf v1.28.1
)
