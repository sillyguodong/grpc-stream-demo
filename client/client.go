package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	streamv1 "gitea.com/sillyguodong/grpc-stream-demo/gen/stream/v1"
	"gitea.com/sillyguodong/grpc-stream-demo/gen/stream/v1/streamv1connect"
	"github.com/bufbuild/connect-go"
)

func main() {
	client := streamv1connect.NewStreamTestServiceClient(
		http.DefaultClient,
		"http://localhost:8080",
	)

	stream, err := client.StreamTest(context.Background(), connect.NewRequest(&streamv1.StreamTestRequest{Content: "This is a message from client."}))
	if err != nil {
		log.Fatalf("client call server error: %v", err)
	}

	count := 0
	for stream.Receive() {
		if stream.Err() != nil {
			fmt.Printf("stream error: %v", err)
		}

		resp := stream.Msg()
		fmt.Println("stream receive:", resp.Content)
		count++
	}

	stream.Close()
	fmt.Printf("Total: %d\n", count)
}
